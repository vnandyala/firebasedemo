//
//  SignUpViewController.swift
//  FirebaseDemo
//
//  Created by Silsila Patra on 25/04/19.
//  Copyright © 2019 Silsila Patra. All rights reserved.
//

import UIKit

enum SignupStrings: String {
    case done = "Done"
    case cancel = "Cancel"
    case dateFormat = "dd/MM/yyyy"
    case chooseImage = "Choose Image"
    case camera = "Camera"
    case gallery = "Gallery"
    case warning = "Warning"
    case dontHaveCamera = "You don't have camera"
}

/// This calss will help user for registering account with firebase.
class SignUpViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: - Variables
    ///
    let datePicker = UIDatePicker()
    ///
    var imagePicker = UIImagePickerController()
    
    // MARK: - IBOutlet
    ///
    @IBOutlet weak var profileImage: UIImageView!
    ///
    @IBOutlet weak var firstNameTextField: UITextField!
    ///
    @IBOutlet weak var lastNameTextField: UITextField!
    ///
    @IBOutlet weak var emailTextField: UITextField!
    ///
    @IBOutlet weak var dobTextField: UITextField!
    ///
    @IBOutlet weak var passwordTextField: UITextField!
    ///
    @IBOutlet weak var newPasswordTextField: UITextField!
    ///
    @IBOutlet weak var profileButton: UIButton!
    
    // MARK: - Life cycle Methods
    
    ///
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        showDatePicker()
        imagePicker.delegate = self
    }
    
    
    // MARK: - User Actions
    ///
    @IBAction func onSignupButtonTap(_ sender: Any) {
    }
    
    ///
    @IBAction func onProfileButtonTap(_ sender: UIButton) {
        //        self.profileButton.setTitleColor(UIColor.white, for: .normal)
        self.profileButton.isUserInteractionEnabled = true
        
        let alert = UIAlertController(title: SignupStrings.chooseImage.rawValue, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: SignupStrings.camera.rawValue, style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: SignupStrings.gallery.rawValue, style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: SignupStrings.cancel.rawValue, style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = sender
            alert.popoverPresentationController?.sourceRect = sender.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Helper Methods
    ///
    func setupUI() {
        profileImage.layer.cornerRadius = 50
    }
    
    ///
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: SignupStrings.warning.rawValue, message: SignupStrings.dontHaveCamera.rawValue, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    ///
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            // imageViewPic.contentMode = .scaleToFill
            profileImage.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

/// Date picker setup.
extension SignUpViewController {
    
    func showDatePicker() {
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: SignupStrings.done.rawValue, style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: SignupStrings.cancel.rawValue, style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        dobTextField.inputAccessoryView = toolbar
        dobTextField.inputView = datePicker
        
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = SignupStrings.dateFormat.rawValue
        dobTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}
