//
//  FirebaseManager.swift
//  FirebaseDemo
//
//  Created by Divakar Reddy Pala on 25/04/19.
//  Copyright © 2019 Venkata Santhi Nandyala. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import FirebaseStorage
typealias CreateUserHandler = (_ isSucess:Bool, _ error:Error?)->Void
class FirebaseManager: NSObject {
    
    enum FireBaseNodes:String{
        case user = "User"
        case profileImage = "ProfileImage"
    }
    static let db = Firestore.firestore()
    static let fireStorage =   Storage.storage().reference()

    static func createUser(with email:String,password:String,gender:String = "",lastName:String,firstName:String,profileImageData:Data? = nil,dob:String,completionHander:@escaping CreateUserHandler){
        
        Auth.auth().createUser(withEmail: email, password: password) { (authData, error) in
            if error != nil {
                completionHander(false,error)
            }else{
                guard let firebaseUser =   authData?.user,let profileData = profileImageData else {
                   return completionHander(false,error)
                }
                uploadImageData(for: firebaseUser.uid, data: profileData, completion: { (photoUrl, err) in
                    if error != nil {
                        completionHander(false,error)
                    }
                    saveUserDetails(uid: firebaseUser.uid, email: email, password: password, photoUrl: photoUrl!, dob: dob, gender: gender, completionHander: completionHander)
                })
            }
        }
    }
    

}

extension FirebaseManager{
    
    static func uploadImageData(for uid:String,data:Data,completion:@escaping (_ url:String?,_ error:Error?)->Void){
        
        let storageRef = fireStorage.child(FireBaseNodes.profileImage.rawValue)

        storageRef.putData(data, metadata: nil) {  (metadata, error) in
            
            print("Fullsize image is stored")
            
            // Get the fullsize downloadURL
            storageRef.downloadURL(completion: { (url, error) in
                if let downloadURL = url?.absoluteString {
                    completion(downloadURL,nil)
                }else{
                    completion(nil,error)

                }
            })
        }
    }
    static func saveUserDetails(uid:String,email:String,password:String,photoUrl:String,dob:String,gender:String,completionHander:@escaping CreateUserHandler){
        
        let userJson = ["uid":uid,"email":email,"password":password,"photo_url":photoUrl,"dob":dob,"gender":gender]
        db.collection(FireBaseNodes.user.rawValue).addDocument(data: userJson) { (err) in
            if err == nil {
                completionHander(true,nil)

            }else{
                completionHander(false,err)
            }
        }
    }
}
